<?php
/**
 * Created by PhpStorm.
 * User: Perseus
 * Date: 10/3/14
 * Time: 3:13 PM
 */


require_once 'swiftmailer/swiftmailer/lib/swift_required.php';
function sendNewsletter($email) {
    $subject = "Good News! You're In!";
    $from = array('do-not-reply@hsn.ph' => 'Home Shopping Network');
    $to = array($email);
    $newsletterFile = 'template/newsletter.html';
    $handle = fopen($newsletterFile, 'r');
    $content = fread($handle, filesize($newsletterFile));
    fclose($handle);

    $message = Swift_Message::newInstance()
        ->setSubject($subject)
        ->setFrom($from)
        ->setTo($to)
        ->setBody($content, 'text/html');

    $transport = Swift_SmtpTransport::newInstance('smtp.sendgrid.net', 465, "ssl")
        ->setUsername('galoredev')
        ->setPassword('ratmaxi8');

    $mailer = Swift_Mailer::newInstance($transport);

    return $mailer->send($message);
}