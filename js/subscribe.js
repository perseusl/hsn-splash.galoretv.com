/**
 * Created by Perseus on 9/29/14.
 */

$("#subscribe_form").submit(function(){
    var subscribers_email = document.getElementById('email').value;
    if(subscribers_email)
    {
        var email = subscribers_email;
        $('.msg-loader').show();
        $('#msg').html("<img src='/img/loader.gif' />");
            //$("#email").readOnly();
            $.ajax({
                type: "POST",
                url: "subscribe.php",
                data: {
                    email: email,
                    submit: 1
                },
                success: function(data) {
                    console.log(data);
                    try{
                        var dataJson = $.parseJSON(data);
                        console.log(dataJson);
                        if(dataJson.message == "Email already exist!")
                        {
                            //$('.msg-loader').show();
                            $('#email').val('');
                            $('#msg').empty();
                            $('#msg').html("Email address already exists!!");
                            $('#msg').css("margin-left", "0");
                            $('#msg').css("margin-top", "0");
                            var marg_left = "-"+$('#msg').width()/2+"px";
                            var marg_top = "-"+$('#msg').height()/2+"px";
                            $('#msg').css("margin-left", marg_left);
                            $('#msg').css("margin-top", marg_top);
                           setTimeout(function(){
                                $('.msg-loader').hide();
                                $('#msg').css("margin-left", "0");
                                $('#msg').css("margin-top", "0");
                            },5000);

                        }
                        else if(dataJson.message == "Ouch! There's something wrong with the server."){
                            $('#email').val('');
                            $('#msg').empty();
                            $('#msg').html("<p>Something went wrong with your transaction,</p><p>Please try again!</p>");
                            $('#msg').css("margin-left", "0");
                            $('#msg').css("margin-top", "0");
                            var marg_left = "-"+$('#msg').width()/2+"px";
                            var marg_top = "-"+$('#msg').height()/2+"px";
                            $('#msg').css("margin-left", marg_left);
                            $('#msg').css("margin-top", marg_top);
                            setTimeout(function(){
                                $('.msg-loader').hide();
                                $('#msg').css("margin-left", "0");
                                $('#msg').css("margin-top", "0");
                            },5000);
                        }
                        else{
                            //$('.msg-loader').show();
                            $('#email').val('');
                            $('#msg').empty();
                            $('#msg').html("<p>EMAIL ADDRESS SAVED!</p><p>A message has been sent into your inbox.</p><br /><br /><p>Have a great day!</p>");
                            $('#msg').css("margin-left", "0");
                            $('#msg').css("margin-top", "0");
                            var marg_left = "-"+$('#msg').width()/2+"px";
                            var marg_top = "-"+$('#msg').height()/2+"px";
                            $('#msg').css("margin-left", marg_left);
                            $('#msg').css("margin-top", marg_top);
                            setTimeout(function(){
                                $('.msg-loader').hide();
                                $('#msg').css("margin-left", "0");
                                $('#msg').css("margin-top", "0");
                            },5000);

                        }
                    }
                    catch (e){
                        //$('.msg-loader').show();
                        $('#email').val('');
                        $('#msg').empty();
                        $('#msg').html("<p>Something went wrong,</p><p>Please try again!</p>");
                        $('#msg').css("margin-left", "0");
                        $('#msg').css("margin-top", "0");
                        var marg_left = "-"+$('#msg').width()/2+"px";
                        var marg_top = "-"+$('#msg').height()/2+"px";
                        $('#msg').css("margin-left", marg_left);
                        $('#msg').css("margin-top", marg_top);
                        setTimeout(function(){
                            $('.msg-loader').hide();
                            $('#msg').css("margin-left", "0");
                            $('#msg').css("margin-top", "0");
                        },5000);
                    }

                }
            });
        /*setTimeout(function(){
            $('.msg-loader').hide();
        },5000);*/
    }
    else
    {
        //$('.msg-loader').show();
        $('#email').val('');
        $('#msg').empty();
        $('#msg').html("Please Enter your email address");
        $('#msg').css("margin-left", "0");
        $('#msg').css("margin-top", "0");
        var marg_left = "-"+$('#msg').width()/2+"px";
        var marg_top = "-"+$('#msg').height()/2+"px";
        $('#msg').css("margin-left", marg_left);
        $('#msg').css("margin-top", marg_top);
        setTimeout(function(){
            $('.msg-loader').hide();
            $('#msg').css("margin-left", "0");
            $('#msg').css("margin-top", "0");
        },5000);
    }
    return false;
});